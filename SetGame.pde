import java.util.Collections;

ArrayList <Card> deck;
ArrayList <Card> inGame;
ArrayList <Card> selected;
ArrayList <Card> sets;
boolean isEnded;

void setup () {
  size (displayWidth, displayHeight);
  deck = new ArrayList <Card>();
  inGame = new ArrayList <Card>();
  selected = new ArrayList <Card>();
  sets = new ArrayList <Card>();
  isEnded = false;
  for (int i = 0; i < 3; i = i+1){
    // determine color
    CardColor c;
    switch (i) {
      case 0:
        c = CardColor.RED;
        break;
      case 1:
        c = CardColor.GREEN;
        break;
      case 2:
      default:
        c = CardColor.BLUE;
        break;
    }
    
    for (int j = 0; j < 3; j = j+1){
      CardFilling f;
      switch (j) {
        case 0:
          f = CardFilling.EMPTY;
          break;
        case 1:
          f = CardFilling.FULL;
          break;
        case 2:
        default:
          f = CardFilling.HALF;
          break;
      }
      
      for (int k = 0; k < 3; k = k+1){
        CardShape s;
        switch (k) {
          case 0:
            s = CardShape.CIRCLE;
            break;
          case 1:
            s = CardShape.SQUARE;
            break;
          case 2:
          default:
            s = CardShape.TRIANGLE;
            break;
        }
       
        for (int l = 0; l < 3; l = l+1){
          CardAmount a;
          switch (l) {
            case 0:
              a = CardAmount.ONE;
              break;
            case 1:
              a = CardAmount.TWO;
              break;
            case 2:
            default:
              a = CardAmount.THREE;
              break;
          }
          Card card = new Card(c, f, s, a);
          deck.add (card);
        }
      }
    }
  }
  
  // shuffle cards
  Collections.shuffle(deck);
  
  // put cards in game
  for (int i = 0; i < 12; i = i+1){
    Card card = deck.remove (0);
    inGame.add (card);
  }
  
}

boolean sketchFullScreen () {
  return false;
}

void draw () {
  background (255);
  fill (0);
  
  // draw cards
  for (int i = 0; i < inGame.size();i = i + 1) {
    Card card = inGame.get (i);
    card.draw(50+190*(i/3), 50+190*(i%3));
  }
  
  // check for sets
  if (selected.size () >= 3){
    if (isSet(selected)){
      for (int i = 0; i < selected.size (); i = i+1) {
        Card card = selected.get (i);
        sets.add (card);
        inGame.remove (card);
      }
    } else {
      javax.swing.JOptionPane.showMessageDialog(null, "Aaahw no Set");
    }
    for (int i = 0; i < selected.size (); i = i+1) {
      selected.get (i).selected = false;
    }
    selected.clear ();
  }
  
  // fill up cards if needed
  if (shouldAddCards(inGame,deck)){
    for (int i = 0; i < 3; i = i+1){
      Card card = deck.remove (0);
      inGame.add (card);
    }
  } else if(!containsAnySet(inGame) && !isEnded){
    javax.swing.JOptionPane.showMessageDialog(null, "GAME OVER");
    isEnded = true;
  }
}

void mouseClicked () {
  for (int i = 0; i < inGame.size (); i = i + 1) {
    Card card = inGame.get (i);
    if ((mouseX > (50+190*(i/3))) && (mouseX < (140+190*(i/3))) 
    && (mouseY > (50+190*(i%3))) && (mouseY < (230+190*(i%3)))){
//      javax.swing.JOptionPane.showMessageDialog(null, "position" + mouseX+ ","+ mouseY);
      card.selected = !card.selected;
      if (card.selected) {
        selected.add (card);
      }
      else {
        selected.remove (card);
      }
    }
     
    
  }
}
boolean isSet(ArrayList <Card> candidates){
  if(((candidates.get(0).c == candidates.get(1).c && candidates.get(1).c == candidates.get(2).c)
    || (candidates.get(0).c != candidates.get(1).c && candidates.get(1).c != candidates.get(2).c && candidates.get(0).c != candidates.get(2).c))
    && ((candidates.get(0).f == candidates.get(1).f && candidates.get(1).f == candidates.get(2).f)
    || (candidates.get(0).f != candidates.get(1).f && candidates.get(1).f != candidates.get(2).f && candidates.get(0).f != candidates.get(2).f))
    && ((candidates.get(0).s == candidates.get(1).s && candidates.get(1).s == candidates.get(2).s)
    || (candidates.get(0).s != candidates.get(1).s && candidates.get(1).s != candidates.get(2).s && candidates.get(0).s != candidates.get(2).s))
    && ((candidates.get(0).a == candidates.get(1).a && candidates.get(1).a == candidates.get(2).a)
    || (candidates.get(0).a != candidates.get(1).a && candidates.get(1).a != candidates.get(2).a && candidates.get(0).a != candidates.get(2).a))) {
    return true; 
  } else {
    return false;
  }
}

boolean shouldAddCards(ArrayList <Card> inGame, ArrayList <Card> deck) {
  if (deck.size() < 3){
    return false;
  }
  if (inGame.size() < 12){
    return true;
  }
  if (containsAnySet(inGame)){
    return false;
  }
  return true;
}

boolean containsAnySet(ArrayList <Card> inGame) {
  for (int i = 0; i < inGame.size (); i = i + 1) {
    for (int j = i + 1; j < inGame.size (); j = j + 1) {
      for (int k = j + 1; k < inGame.size (); k = k + 1) {
        ArrayList <Card> candidates = new ArrayList <Card>();
        candidates.add(inGame.get(i));
        candidates.add(inGame.get(j));
        candidates.add(inGame.get(k));
        if (isSet(candidates)){
          return true;
        }
      }
    }
  }
  
  return false;
}

enum CardColor {
  RED, GREEN, BLUE
};

enum CardFilling {
  EMPTY, FULL, HALF
};

enum CardShape {
  CIRCLE, SQUARE, TRIANGLE
};

enum CardAmount {
  ONE, TWO, THREE
};

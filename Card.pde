class Card {
  CardColor c;
  CardFilling f;
  CardShape s;
  CardAmount a;
  boolean selected;
  // hieronder is de functie voor het aanmaken van een kaart: constructor (functie voor nieuwe dingen maken)
  Card (CardColor initialC, CardFilling initialF, CardShape initialS, CardAmount initialA){
    c = initialC;
    f = initialF;
    s = initialS;
    a = initialA;
    selected = false;
  }
  // hieronder functie van de kaart om getekend te worden (void: geen antwoord, boolean: antwoord ja of nee)
  void draw (int x, int y){
    // draw card
    if (selected){
      strokeWeight (3);
    }
    else { 
      strokeWeight (1);
    }
    stroke (0);
    fill (255);
    rectMode (CORNER);
    rect (x, y, 90, 180);
    
    // draw symbol
    color cardColor;
    if (c == CardColor.RED){
      cardColor = color (255,0,0);
    }
    else if (c == CardColor.GREEN){
      cardColor = color (0,255,0);
    }
    else {
      cardColor = color (0,0,255);
    }
    if (f == CardFilling.EMPTY) {
      fill (255);
    } 
    else {
      fill (cardColor);
    }
    stroke (cardColor);
    strokeWeight (3);
    int cardAmount;
    if (a == CardAmount.ONE){
      cardAmount = 1;
    }
    else if (a == CardAmount.TWO){
      cardAmount = 2;      
    }
    else {
      cardAmount = 3;
    }
    for (int i = 0; i < cardAmount; i = i + 1) {  
      if (s == CardShape.CIRCLE){
        ellipse (x+45, y+50+(i*40),70,20);
      }
      else if (s == CardShape.SQUARE){
        rect (x+10,y+40+(i*40),70,20);
      }
      else {
        triangle (x+45,y+40+(i*40), x+10, y+60+(i*40),x+80,y+60+(i*40));
      }
    }
    
    fill(255);
    stroke(255);
    if (f == CardFilling.HALF) {
     for (int j = (x+10); j < (x+90); j = j+5){
       for (int k = (y+20); k < y+160; k = k+5) {
         point (j,k);
       }
     }
    }
  }
 
};


